# Create your views here.
import logging
import os

import pandas as pd
from django.shortcuts import render, HttpResponse, redirect
from zp.models import CategoryModel

logger = logging.getLogger('django')
from DataView.settings import BASE_DIR
from django.views.decorators.cache import cache_page
from django.views.decorators.clickjacking import xframe_options_exempt


def readFile(filename, chunk_size=512):
    """
    缓冲流下载文件方法
    :param filename:
    :param chunk_size:
    :return:
    """
    with open(filename, 'rb') as f:
        while True:
            c = f.read(chunk_size)
            if c:
                yield c
            else:
                break
@cache_page(60 * 60*24)
def page_not_found(request,exception):
    return render(request, '404.html',context={'title':'404'})
@cache_page(60 * 60*24)
def sever_error(request):
    return render(request, '500.html',context={'title':'500'})

def get_echarts_all_by_zwyx_value(x,key):
    return pd.Series({key: x[key].tolist()[0], 'max_zwyx': x['max_zwyx'].max(), 'min_zwyx': x['min_zwyx'].min(), 'count': x['count'].sum()})

@cache_page(60 * 60*24)
def home(request):
    category_list=CategoryModel.objects.all()
    return render(request, 'index.html',context={'title':'首页','category_list':category_list})

@cache_page(60 * 60*24)
def no_found(request):
    zwlb=request.GET.get('zwlb','')
    return render(request,'no_found.html',context={'title':'未找到','zwlb':zwlb})

def go_redict(zwlb,path):
    try:
        if not os.path.exists(os.path.join(BASE_DIR, 'templates/{}'.format(path))):
            return redirect('/no_found/?zwlb={}'.format(zwlb))
        else:
            return HttpResponse(readFile(os.path.join(BASE_DIR, 'templates/{}'.format(path))))
    except Exception as e:
        logger.error(str(e))

# 地点薪资关系图(地图+柱状)
@cache_page(60 * 60 * 24)
@xframe_options_exempt
def zwyx_dd(request):
    try:
        zwlb = request.GET.get('zwlb', '')
        if zwlb:
            path=f'zwyx_dd/{zwlb}.html'
        else:
            path = 'zwyx_dd.html'
        return go_redict(zwlb,path)
    except Exception as e:
        logger.error(str(e))

# 学历 薪资+职位量关系图
@cache_page(60 * 60 * 24)
@xframe_options_exempt
def zwyx_xl(request):
    zwlb = request.GET.get('zwlb', '')
    if zwlb:
        path=f'zwyx_xl/{zwlb}.html'
    else:
        path = 'zwyx_xl.html'
    return go_redict(zwlb,path)

# 公司规模 薪资+职位量关系图
@cache_page(60 * 60 * 24)
@xframe_options_exempt
def zwyx_gsgm(request):
    zwlb = request.GET.get('zwlb', '')
    if zwlb:
        path = f'zwyx_gsgm/{zwlb}.html'
    else:
        path = 'zwyx_gsgm.html'
    return go_redict(zwlb,path)

# 公司性质 薪资+职位量关系图
@cache_page(60 * 60 * 24)
@xframe_options_exempt
def zwyx_gsxz(request):
    zwlb = request.GET.get('zwlb', '')
    if zwlb:
        path = f'zwyx_gsxz/{zwlb}.html'
    else:
        path = 'zwyx_gsxz.html'
    return go_redict(zwlb,path)

# 各就业方向职位月薪和招聘人数
@cache_page(60 * 60 * 24)
@xframe_options_exempt
def zwyx_zw_count(request):
    zwlb = request.GET.get('zwlb', '')
    if zwlb:
        path = f'zwyx_zw_count/{zwlb}.html'
    else:
        path = 'zwyx_zw_count.html'
    return go_redict(zwlb, path)


# 公司行业
@cache_page(60 * 60 * 24)
@xframe_options_exempt
def zwyx_gshy(request):
    zwlb = request.GET.get('zwlb', '')
    if zwlb:
        path = f'zwyx_gshy/{zwlb}.html'
    else:
        path = 'zwyx_gshy.html'
    return go_redict(zwlb,path)

# 职位类型
@cache_page(60 * 60 * 24)
@xframe_options_exempt
def zwyx_type(request):
    zwlb = request.GET.get('zwlb', '')
    if zwlb:
        path = f'zwyx_type/{zwlb}.html'
    else:
        path = 'zwyx_type.html'
    return go_redict(zwlb, path)


@cache_page(60 * 60 * 24)
@xframe_options_exempt
def zp_word(request):
    zwlb = request.GET.get('zwlb', '')
    if zwlb:
        path = f'zp_word/{zwlb}.html'
    else:
        path = 'zp_word.html'
    return go_redict(zwlb, path)
